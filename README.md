# Place genomes in whole genome backbone tree

Workflow to place query genomes in a phylogenetic context. Tree contains a representative set of 
bacterial and archaeal genomes and is based on a concatenated alignment of 56 universal marker proteins.

### 1\. Install dependencies  
You can install any of the following dependencies by following "SETUP.md" under the "Source" tab.  

#### Setting environment
These are the versions I used but other ones will probably work  
```
# general dependencies
module load python/2.7.4 biopython/1.61 prodigal/2.50 hmmer/3.0

# mafft
module load mafft/7.221

# FastTreeMP,raxml & parallel
I made symlinks to required software under our sandbox so you can add to PATH
export PATH=/global/projectb/sandbox/RD/FredrickTreeDependencies:$PATH 
```

### 2\. Create directory of query genomes  
Make a directory called, for instance, "Genomes", and put the query genomes in there that you want placed onto the tree.  Don't have other files or directories under here that are not one of your query genomes since they will probably crash the program.

### 3\. Run tree_wrapper_NERSC.sh

```
tree_wrapper_NERSC.sh [options] <directory of your genomes>  

   required:
    <directory of your genomes>
    <-m reference marker proteins [56COGs|RNAPol|RProt|<custom>]> 
    <-d reference database to use [imggRNPOL65|refSSU85|refseqRNPOL90|<custom>]>
    note: the -m and -d arguments are used to specify which COGs 
          are to be used for hmm search

   alignment options:
    <-a an alignment command[mafft|ete3|hmmalign]>
    <-l generate tree from user supplied genomes only (i.e. makes multiple alignment of your genomes only)> 

   tree options:
    <-t a tree building command[fasttree|raxml|pplacer]>

   global options
    <-p number threads [8] for multithreading on fasttree (and parallel jobs: mafft & prodigal)> 
    <-c to run the classifier>
```   

##### Notes: 
* The argument for number of processes is used for fasttree but also dictates the number prodigal and mafft jobs that can run in parallel which is capped at 8.  If RAxML is chosen, 16 processes are used on an "exclusive.c" machine and this is hardcoded. Parallel jobs uses "parallel GNU"
* hmmalign is a fast option to create alignments and is the more accurate if your genomes are not too novel (i.e no close relative in NCBI genbank)
* Running fasttree is faster but doesn't produce very good confidence values.
* raxml requires NERSC's compute cluster  

Below are the choices you have for -m <markers> and -d <database>. Both flags must be used together.  

* markers: [56COGs|RNAPol|RProt|<custom>]  
* database: [imggRNPOL65|refSSU85|refseqRNPOL90|<custom>]  

#### description of markers  
56COGs:  56 marker genes curated by Frederik Schulz
RNAPol:  3 RNA polymerase genes (traditionally used for trees)
RProt:   30-31 genes ???
RProtBF: 16 genes ???

#### description of databases  
  
##### imggRNPOL65: 
 * December 2016  
 * All bacterial and archaeal genomes in IMG  
	* Filtered based on presence of > 40 (out of 56) marker proteins (56COGs) and no more than 2 markers duplicated  
	* Clustered at 65% sequence similarity of RNA polymerase beta subunit/160 kD subunit (COG0086)  
	* Comprises 424 representative genomes  
  
##### refSSU85:  
 * April 2016  
 * All genomes in NCBI Genbank  
	* Clustered at 85% sequence similarity of SSU rRNA gene  
        * Comprises 575 representative genomes  
  
##### refseqRNPOL90:  
 * July 2016  
 * All NCBI Genbank reference genomes  
	* Clustered at 90% sequence similarity of RNA polymerase beta subunit/160 kD subunit (COG0086)  
        * Comprises 2069 representative genomes  
  
##### custom:  
 * Not implemented yet  

#### Examples
#### example1: _your genomes on reference tree_ 
runs mafft and then fasttree using your genomes in "Genomes".  The parallel jobs are limited here to 4 processes.   
`tree_wrapper_NERSC.sh -a mafft -t fasttree -m 56COGs -d refSSU85 -p 4 Genomes`  

#### example2: _tree of your genomes only_  
using the -l flag will only align your genomes against each other and omit the reference genomes. Thus only your genomes will be on the tree. When you use the -l flag, you don't need "-m".  By default, mafft and prodigal will be run in parallel using 8 processes.  
`tree_wrapper_NERSC.sh -a mafft -t fasttree -l Genomes`  

#### example3: _classification_  
using -c will classify your genomes based on where they land in the tree. This is incompatible with -l flag.  You may include the -x argument specifying the taxa level to classify to where phyla (P) is default.  I'm classifying to class level here.  Once you're finished, look for a file called something like 'querynames.classified.C'  
`tree_wrapper_NERSC.sh -a mafft -t fasttree -c -x C -m 56COGs -d refseqRNPOL90 Genomes`  

#### example4: _make tree with supplied alignment_  
this will run a tree only.  You must have an alignment file called: "concat.faa" in fasta format (phylip may or may not work).  
`tree_wrapper.sh -t raxml Genomes`  

### 4\. Results
If you called your directory of genomes "Genomes", there should be a new directory called `Genomes_tree` which has a bunch of temp files that you don't need except for debugging purposes.  The important files are the alignment in fasta format `<something>.*.trim.aln`, the fasttree tree `<something>.01.WAG.nwk` or the two raxml trees `RAxML_bipartitions.nwk3` & `RAxML_bipartitionsBranchLabels.nwk3`

### 5\. Bonus figures
If you want a pdf of the tree in resplendent colors, you can run this in an x-window
   ```
   plot_tree_ete3.py *.nwk 
   <path_to_repository>/lookup/ABtaxalookup.V2 
   <path_to_repository>/lookup/phyla_colors.tab  
   ```

-------------------------------------

## Contributors
For questions about the alignment or tree algorithms contact:  
Frederik Schulz <fschulz@lbl.gov\>

To report bugs, contact:  
Jeff Froula <jlfroula@lbl.gov\>