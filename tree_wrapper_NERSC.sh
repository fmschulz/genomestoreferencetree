#!/bin/bash -l
#
# Purpose
# -------
# Place genomes in whole genome backbone tree
#
# Assumptions
# -----------
# Make a directory called, for instance, "Genomes", and put 
# your genomes in there that you want placed onto the tree

CMD_ALIGN=
CMD_TREE=
CMD_CLASSIFICATION=
THREADS=
FASTTREE=
CLEAN=
MYGENOMESONLY=
GENOMES=
LOG=
path_to_bin=
MYGENOMES_FAA=
DATABASE= 
MARKERS=
SUPPLIED_ALIGNMENT=

path_to_repository=$(realpath $0)
path_to_repository=$(dirname $path_to_repository)
path_to_bin="$path_to_repository/bin"

# this imports all bash functions
. $path_to_bin/tree_functions

## -----------------------------------------------------------------------##
## check for all required args
## -----------------------------------------------------------------------##
ARGS=($*)
if [[ ! $ARGS ]]; then 
    echo -e "Missing some required arguments\n"
    usage_main
fi

while getopts 'a:t:m:p:d:e:lc' OPTION
do 
  case $OPTION in 
  a)    CMD_ALIGN="$OPTARG"
        ;;  
  t)    CMD_TREE="$OPTARG"
        ;;  
  m)    MARKERS="$OPTARG"
        ;;  
  p)    THREADS="$OPTARG"
        ;;
  d)    DATABASE="$OPTARG"
        ;;
  e)    SUPPLIED_ALIGNMENT="$OPTARG"
        ;;
  c)    CMD_CLASSIFICATION=1
        ;;
  l)    MYGENOMESONLY=1
        ;;
  ?)    usage_main
        exit 1
        ;; 
		esac
done

shift $((OPTIND - 1))
GENOMES=($@)
if [ ! $GENOMES ]; then
	echo "Missing directory of genomes"
	usage_main
fi
if [[ ${#GENOMES[@]} > 1 ]]; then
	echo "## There should only be one directory name (at end of command line) specifying where genomes are"
	exit
fi

if [ ! -d $GENOMES ]; then
	echo
	echo "The directory \"$GENOMES\" does not seem to exist"
	exit 1
fi



## -----------------------------------------------------------------------##
## Prepare environment and variables
## -----------------------------------------------------------------------##
GENOMES=$(realpath $GENOMES)
dir_bname=${GENOMES##*/}
outdir="${dir_bname}_tree"
THREADS=${THREADS:=8}
LOG=run.log
CMD_ALIGN=${CMD_ALIGN:='none'}
CMD_TREE=${CMD_TREE:='none'}
MARKERS=${MARKERS:='none'}
MYGENOMES_FAA=MyGenomeHits56COGs_faa
GOLD_HMMS="$path_to_repository/hmms/$MARKERS"
#GOLD_ALIGNMENTS="$path_to_repository/databases/aln_initial"

## -----------------------------------------------------------------------##
## Start testing that we have required arguments and other pre-requisite tables
## -----------------------------------------------------------------------##

# make the directory where all results will be
if [ ! -d $outdir ]; then
	mkdir $outdir 
fi

if [[ $MARKERS != 'none' ]]; then
	# If we set -m <MARKERS> then we need -d <DATBASE>
	if [[ ! $DATABASE ]]; then
        cat <<-EOF
	    If you set -m <MARKERS> then you need -d <DATBASE>
        Even if you are running a local tree (i.e. -l flag) we need 
        a database name. For example, the markers 56COGs can be slightly different 
        on which reference database was used to derive them.
		EOF
	    exit 1
	fi
#    if [[ $DATABASE == 'refSSU85' ]] || [[ $DATABASE == 'refseqRNPOL90' ]] ; then
#		echo "here";exit
#		DATABASE_FULL="$path_to_repository/databases/$DATABASE"
#    	if [[ ! -d $DATABASE_FULL ]]; then
#    		echo "Failed to find database directory: $DATABASE_FULL"
#    		exit 1
#    	fi
#    else
#    	# This should be a custom database, check if its legit
#		echo "
#    	checkLegitimacy $DATABASE_FULL | exit 1
#		
#    fi

	if [[ ! $MARKERS == '56COGs' ]] && [[ ! $MARKERS == 'RNAPol' ]] && [[ ! $MARKERS == 'RProt' ]] && [[ ! $MARKERS == 'RProtBF' ]] && [[ ! $MARKERS == 'gtdb_bac' ]] && [[ ! $MARKERS == 'gtdbbac' ]]; then
		echo "You need to restrict your argument to one of these [56COGs|RNAPol|RProt|RProtBF|gtdb_bac|gtdbbac]. "
		echo "Refer to manual for more information about choices."
		exit 1
	fi

	# get full path to MARKERS
	MARKERS_FULL="$path_to_repository/databases/$DATABASE/$MARKERS"
	# directory structure is
	# DATABASE: refSSU85/      refseqRNPOL90
	# MARKERS: 56COGs/  RNAPol/  RProt/  RProtBF
	if [[ ! -d $MARKERS_FULL ]]; then
		echo "Failed to find marker directory: $MARKERS_FULL"
		exit 1
	fi
fi


# if we're running classification or a tree, then
# check that we have $MARKERS set and that there is a $LOOKUP table
if [ $CMD_CLASSIFICATION ] || [[ $CMD_TREE != 'none' ]]; then
    if [[ $MARKERS != 'none' ]]; then
		LOOKUP="$path_to_repository/databases/$DATABASE/lookup.tab"
    	if [[ ! -e "$LOOKUP" ]]; then
    		echo "No lookup table found: $LOOKUP"
    		exit 1
    	fi
	else
		echo "You need to include -m AND -d when you use -c or -t" 
		exit 1
    fi
fi

# Set the name for the ALIGNMENT file depending on 
# conditions
if [[ $CMD_ALIGN != 'none' ]]; then
	ALIGNMENT="${MARKERS}_${DATABASE}.aln"
else
	# if we're runnign a tree but not an alignment, then
	# check for supplied alignment.
    if [[ $CMD_TREE != 'none' ]]; then
    	if [[ ! $SUPPLIED_ALIGNMENT ]]; then
    		echo "You need to supply a multiple alignment by supplying an argument to \"-e\"."
			echo "It has to be in fasta format (i.e. one you previously created by using \"-a\" flag (${dir_bname}_tree/*.aln))"
    		exit 1
    	fi
		# we need to copy the user 
		# supplied alignment over unless it already exists in current working dir.
		SUPPLIED_ALIGNMENT=$(realpath $SUPPLIED_ALIGNMENT)
		ALIGNMENT_BASENAME=$(basename $SUPPLIED_ALIGNMENT)
		if [[ ! -s $outdir/$ALIGNMENT_BASENAME ]]; then
			cp $SUPPLIED_ALIGNMENT $outdir/$ALIGNMENT_BASENAME
		fi
    	ALIGNMENT=$ALIGNMENT_BASENAME
    fi
fi

echo -e "\n## For any failures, check the log at ${dir_bname}_tree/$LOG\n"

# I don't want some jobs to have more than 8 jobs running in parallel 
# because I'm worried about memory
if [[ "$THREADS" -gt 8 ]]; then
	PROC=8
else
	PROC=$THREADS
fi

#
# load global modules and then those specific to the alignment and tree programs
#
loadRequiredModules $CMD_ALIGN $CMD_TREE 

# check requirements for classification function
if [ $CMD_CLASSIFICATION ]; then
	if [ $MYGENOMES ]; then
		echo "Warning: if you use the \"-l\" flag then you can't run the classifier since to classify your unknown genomes, they need to be placed on the tree that includes the reference genomes and not just a tree of your genomes."
		echo "... skipping classification"
	fi
fi


## -----------------------------------------------------------------------##
##    From here on out, we're in the results directory where all scripts
##    will be run.
## -----------------------------------------------------------------------##
cd $outdir
echo "## changing into $outdir"

# Verify that alignment argument is correct
if [[ $CMD_ALIGN != 'none' ]]; then
	# check tree commands are all understood
    if [[ $CMD_ALIGN != 'mafft' ]] && [[ $CMD_ALIGN != 'ete3' ]] && [[ $CMD_ALIGN != 'hmmalign' ]]; then 
    	echo "please use one of these alignment commands [mafft|ete3|hmmalign]"
    	exit
    fi
fi

# check tree commands are all understood
if [[ $CMD_TREE != 'none' ]]; then
    if [[ $CMD_TREE != 'pplacer' ]] && [[ $CMD_TREE != 'fasttree' ]] && [[ $CMD_TREE != 'raxml' ]]; then
    	echo "please use one of these tree commands [pplaccer|fasttree|raxml]"
    	exit
    fi
fi


#
# protein extraction needs to be done only if we need an alignment
#
if [[ $CMD_ALIGN != 'none'  ]]; then
	if [[ ! $MYGENOMESONLY ]]; then
		if [[ $MARKERS == 'none' ]]; then
			echo "Missing argument for \"-m\". You need to include the directory name for the pre-computed marker proteins. The options are [56COGs|RNAPol|RProt|RProtBF]. (You'll also need to set database for \"-d\" where options are [refSSU85|refseqRNPOL90|<custom>]].)"
			echo "Otherwise you can just use your genomes to do alignment and tree, in which case you want to use -l flag."
			exit 1
		fi
		if [ ! -d $GOLD_HMMS ]; then
			echo "Required directory missing for hmms: $GOLD_HMMS"
			exit 1
		fi
	fi

    extract_proteins $LOG $GENOMES $PROC $MARKERS_FULL $MYGENOMES_FAA $GOLD_HMMS $MYGENOMESONLY
fi

#
# Run the alignment commands
#
case "$CMD_ALIGN" in
	mafft)
		run_mafft $LOG $ALIGNMENT || exit 1
		;;
	ete3)
		run_ete3 $LOG $ALIGNMENT || exit 1
		;;
#	hmmalign)
#		run_hmmalign $LOG $MYGENOMES_FAA $GOLD_HMMS $GOLD_ALIGNMENTS $ALIGNMENT || exit 1
#		;;
esac

#
# make sure alignment completed
#
if [[ $CMD_ALIGN != 'none' ]]; then
    if [[ ! -s $ALIGNMENT ]]; then
    	echo "Error: Missing or empty alignment file: $ALIGNMENT."
    	exit 1
    fi
fi

#
# Run the tree commands
#
case "$CMD_TREE" in
	pplacer)
		echo pplacer is not working yet ... || exit 1
		;;
	fasttree)
		run_fasttree $LOG $ALIGNMENT $THREADS || exit 1
		;;
    
	raxml)
		run_raxml $LOG $ALIGNMENT || exit 1
		exit 
		;;
esac

#
# verify tree was made
#
if [[ $CMD_TREE != 'none' ]] || [ $CMD_CLASSIFICATION ]; then
    TREE=($(ls RAxML_bipartitions.nwk* final.*.nwk 2> /dev/null))
    if [[ ${#TREE[@]} > 1 ]]; then
    	echo "Error: We found more than one tree. Please make sure there is only one named like [RAxML_bipartitions.nwk*|final.*.nwk]"
    	echo "${TREE[@]}"
    	exit 1
    elif [[ ${#TREE[@]} == 0 ]]; then
    	echo "Error: No tree was found.  Please make sure there is only one named like [RAxML_bipartitions.nwk*|final.01.WAG.nwk]"
    	print "${TREE[@]}"
    	exit 1
    else
    	echo "## Found final tree: ${TREE[@]}"
    fi
fi

#
# Run the classify command
#
# don't classify if you use -l flag
if [ $CMD_CLASSIFICATION ]; then
	if [[ $MYGENOMESONLY ]]; then
		echo "You can't run the classifier when only using your genomes to make tree (i.e. -l flag)."
		exit
	else
    	QUERYNAMES="querynames"
    	run_classifier $LOG $TREE $QUERYNAMES $LOOKUP 2>> $LOG
    fi
fi

#
# Plot the tree
#
run_treePlotter ${TREE[0]} $LOOKUP 


# clean up tmp files
#rm -r Allgenomes Hits *fna 


