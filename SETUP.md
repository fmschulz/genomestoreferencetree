## Prerequisites to run GenomesToReferencetree

* If working on NERSC, most prerequisites can be loaded as modules, binaries to run the missing prerequisites are included in the bin directory of the repository

* If not on NERSC following prerequisites have to be set up to successful run GenomesToReferencetree.

** Python requirements **

Python 2.7, Python 3 should also work

```
pip install ete3   # or setup using miniconda: http://etetoolkit.org/download/
pip install biopython
```
** Prodigal **

download from https://github.com/hyattpd/Prodigal/releases
```
chmod u+x prodigal.linux
mv prodigal.linux prodigal
```

** hmmer3 **
```
wget ftp://selab.janelia.org/pub/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz
tar xvfz hmmer-3.1b2-linux-intel-x86_64.tar.gz
```
Unzip zipfile and follow instructions in file 'INSTALL'

** mafft ** 

download and follow instructions at http://mafft.cbrc.jp/alignment/software/linux.html

** fasttreeMP **
```
wget http://meta.microbesonline.org/fasttree/FastTreeMP
chmod u+x FastTreeMP
```  
OR to get version with increased precision limits (i.e. more accurate branch lengths), cut and paste C code from  
http://microbesonline.org/fasttree/FastTree.c  

This gives better precision limits: -DUSE_DOUBLE  
This gives multi-threaded functionality: -DOPENMP -fopenmp  
`gcc -DUSE_DOUBLE -O3 -finline-functions -funroll-loops -Wall -o FastTreeMP -lm -DOPENMP -fopenmp FastTree.c`


** trimal **
```
git clone https://github.com/scapella/trimal.git
cd trimal/source
make
```

** parallel **  
make sure $HOME/bin is set to where you want to install parallel
```
wget http://git.savannah.gnu.org/cgit/parallel.git/plain/src/parallel
chmod 755 parallel
cp parallel sem $HOME/bin
```  

** RAxML **  
__note: you need to have openmp installed and the mpicc command in PATH to make Makefile.SSE3.HYBRID.gcc__
```
git clone https://github.com/stamatak/standard-RAxML.git  
cd standard-RAxML/  
make -f Makefile.SSE3.gcc
rm *.o
make -f Makefile.SSE3.HYBRID.gcc
rm *.o
```