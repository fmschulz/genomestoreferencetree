#!/usr/bin/env perl
use strict;
use warnings;

my $taxa_level=$ARGV[0];
die "Usage: $0 <taxa level [kingdom|phyla|class|order|family|genus]>\n" unless $taxa_level;
my %tax_hash = ('kingdom' => -1,
        'phyla' => -2,
        'class' => -3,
        'order' => -4,
        'family' => -5,
        'genus' => -6,
        );

my %uniqueTaxa = ();
#my %short2long = ();
my @colors = ('#4383b7','#ce612f','#197a2b','#f2071a','#9cf400','#008080','#c8d6a2','#8b75a5','#81d8d0','#008080','#779cca','#614126','#003366','#fbc704','#e8c396','#34d151');
my %color_scheme = ('#4383b7' => 'light blue',
   '#e8c396' => 'tan', 
   '#ce612f' => 'rust',
   '#197a2b' => 'dark green',
   '#34d151' => 'light green',
   '#f2071a' => 'bright red',
   '#9cf400' => 'neon green',
   '#008080' => 'blue-green',
   '#c8d6a2' => 'pea green',
   '#8b75a5' => 'grey purple',
   '#81d8d0' => 'turquios',
   '#779cca' => 'light blue',
   '#614126' => 'dark brown',
   '#003366' => 'navy blue',
   '#fbc704' => 'deep yellow',
   '#b6c4d8' => 'grey');

my $colorKey = "color_key";
my @acceptable = qw(kingdom phyla class order family genus);

unless (grep {/$taxa_level/} @acceptable){
    die "$taxa_level is not acceptable taxa name: use: ",join(' ',@acceptable),"\n";
}

my $lookup_combined ="lookup_combined.tab";
if ( -e $lookup_combined ){
    unlink $lookup_combined;
}
open OUT,">>$lookup_combined" || die "$!\n";
open COLOR,">$colorKey" || die "$!\n";


my $taxalookup="Q0_taxalookup.tab";
my $lookup="lookup.tab";
die "Failed to find file: $taxalookup\n" unless -e $taxalookup;
die "Failed to find file: $lookup\n" unless -e $lookup;

open FH,"$taxalookup" || die "$!\n";
while (<FH>){
    chomp;
    my @a=split(/\s+/);
#    my $shortname = shift @a;
    shift @a;
    my @long=split(/\_/,$a[0]);
#    die "@long line: $a[0] short: $shortname\n" unless scalar @long >= 2;
    my $assemblyID = join('_',@long[0..1]);
#    $short2long{$assemblyID} = $shortname;
}
close FH;

open FH,"$lookup" || die "$!\n";
#my $i=0;
while (<FH>){
    chomp;
    my @line=split(/ /);
    my $assemblyID = shift @line;
#    my $shortname;
#    if (exists $short2long{$assemblyID}){
#        $shortname = $short2long{$assemblyID}; 
#    }else{
#        die "no short name found in: $_\n" unless $shortname;
#    }

    my @a=split(/\|/);
    if (exists $tax_hash{$taxa_level} ) {
        if ($a[$tax_hash{$taxa_level}]){
            if ($a[$tax_hash{$taxa_level}] eq "na"){
#                push(@{$uniqueTaxa{"NA_${i}"}},$shortname);
                push(@{$uniqueTaxa{"NA"}},$assemblyID);
#                $i++;
            }else{
#                push(@{$uniqueTaxa{$a[$tax_hash{$taxa_level}]}},$shortname);
                push(@{$uniqueTaxa{$a[$tax_hash{$taxa_level}]}},$assemblyID);
            }
        }else{
            warn "no taxa name found for $taxa_level in line: $_\n";
#            push(@{$uniqueTaxa{"NA_${i}"}},$shortname);
            push(@{$uniqueTaxa{"NA"}},$assemblyID);
#            $i++;
        }
    }else{
        die "failed to find ${taxa_level}(with index $tax_hash{$taxa_level}) in $_\n";
    }

    print OUT "$_\n";
}
close FH;

# for each new taxa name, pick new color
foreach my $taxa (keys %uniqueTaxa){
    my $nextcolor;
    if ($taxa eq "NA"){
        $nextcolor = '#b6c4d8';  # all "NA" genomes should be grey
    }else{
        $nextcolor = shift @colors;
    }
    die "ran out of colors\n" unless $nextcolor;
    print COLOR "$taxa $nextcolor $color_scheme{$nextcolor}\n"; 
    
    print OUT "color:${nextcolor}:",join(' ',@{$uniqueTaxa{$taxa}}),"\n";

}

print OUT "support: no\n";
close OUT;
close COLOR;

#outgroup:Q0ODIHUH Q0FODTFQ Q0RPUZNB Q0BDWWQO Q0QVEPSC Q0IBOQJK Q0DRRYPN

print "wrote to $lookup_combined\nColor key is in $colorKey\n";

