#!/bin/bash -l
#$ -cwd -b yes -j yes -w e -r y'
#$ -l exclusive.c,h_rt=12:00:00
#$ -N aln

list=$1
threads=$2

if [ ! $list ]; then
	echo "Usage: qsub -t 1:x $0 <list of faa> <<threads[16]>>"
	exit
fi

if [ ! -s $list ]; then
	echo "Missing or empty list file: $list"
	exit
fi

tools=$(realpath $0)
tools=$(dirname $tools)
threads=${threads:=16}
file=$(head -n $SGE_TASK_ID $list | tail -1)
bname=$(basename $file)
ID=${bname%%.*}


echo "$tools/xvfb-run -f xauthor.$$.$(echo $RANDOM) -e xvfb.errors --auto-servernum -w 10 $tools/ete3 build -a $file -o ${ID}_ETE --clearall --rename-dup-seqnames -w metaligner_default-trimal01-none-fasttree_full --cpu $threads"
	  $tools/xvfb-run -f xauthor.$$.$(echo $RANDOM) -e xvfb.errors --auto-servernum -w 10 $tools/ete3 build -a $file -o ${ID}_ETE --clearall --rename-dup-seqnames -w metaligner_default-trimal01-none-fasttree_full --cpu $threads

