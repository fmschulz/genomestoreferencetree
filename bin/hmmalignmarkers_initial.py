#!/usr/bin/env python
import sys 
import glob
from Bio import AlignIO
from Bio import SeqIO
from subprocess import call
import os.path

"""Use hmm align to align query faa sequences based on the respective hmm and initial alignment"""
"""fschulz@lbl.gov, August 2016"""

if len(sys.argv) < 2:
    print "\n"
    print 'hmmalignmarkers_initial.py \ '
    print '  <directory of markers(faa)> \ '
    print '  <directory of markers(hmms)> \ '
    print "\n"
    print 'example: '
    print 'hmmalignmarkers_initial.py markers_refSSU85 hmms_all56 refSSU85_aln '
    print "\n"
    sys.exit()

# Dir containing all extracted proteins from all query genomes for each marker
QUERYSEQSDIR = sys.argv[1]
# Dir with the marker hmms
HMMDIR = sys.argv[2]


def filteraln(aln, query):
	"""Only keep querysequences in alignment"""
	with open(query.split("/")[-1].split("_")[0] + ".f.aln", "w") as outfile:
		seqstokeep = []
		for seq_record in SeqIO.parse(query, "fasta"):
			seqstokeep.append(seq_record.description)
		for seq_record in SeqIO.parse(aln, "fasta"):
			if seq_record.description in seqstokeep:
				#SeqIO.write(seq_record, query.split("/")[-1].rsplit(".", 1)[0] + "f.aln", "fasta")
				#print (seq_record,query.split("/")[-1].rsplit(".", 1)[0] + "f.aln")
				outfile.write(">" + str(seq_record.description) + "\n" + str(seq_record.seq) + "\n")

for marker in glob.glob(HMMDIR + "/*hmm"):
	markername = marker.split("/")[-1].rsplit(".", 1)[0]
	print (markername)
	query = str(QUERYSEQSDIR) + "/" + str(markername) + ".merged.faa"

	""" Align with hmmalign"""
	try:
		os.path.isfile(query)
	except IOError, e:
		print e
		continue
		
	call(["hmmalign", "--trim", "-o", markername + ".sto", marker, query])

	aln = AlignIO.read(markername + ".sto", "stockholm")
	AlignIO.write(aln, markername + ".aln", "fasta")
	filteraln(markername + ".aln", query)
	call(["trimal", "-in", markername + ".merged.faa.f.aln", "-out", markername + ".01.aln", "-gt", "0.1"])

#	""" Convert alignment to fasta format"""
#	aln = AlignIO.read(markername + ".sto", "stockholm") 
#	AlignIO.write(aln, markername + ".aln", "fasta")
#
#	"""Optional: only keep query sequences in alignment"""
#	filteraln(markername + ".aln", query)
#	""" Alignment trimming"""
#	call(["trimal", "-in", markername + "f.aln", "-out", markername + ".01.aln", "-gt", "0.1"])

