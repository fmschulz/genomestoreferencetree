#!/usr/bin/env perl
use strict;
use warnings;

my $filetotraslate = $ARGV[0];
my $lookup = $ARGV[1];
die "\nUsage: $0 <newick file to traslate> <lookup table (i.e. short name<tab>ori name) as in Q0_taxalookup.tab>\n\n" 
  unless $filetotraslate && $lookup;
die "Missing or empty: $filetotraslate\n" unless -s $filetotraslate;
die "Missing or empty: $lookup\n" unless -s $lookup;

my %hash = ();

open FH,"$lookup" || die "$!\n";
while (<FH>){
	chomp;
    # short name must be first column
	my @line = split(/\s+/);

	$hash{$line[0]}=$line[1];

}
close FH;

open FH,"$filetotraslate" || die "$!\n";
my @wholefile = (<FH>);
my $asString = join('',@wholefile);

# do replacements all in one go using sed
#$asString =~ s/([(,])([ABQ]0.*?):/${1}$hash{$2}:/g;
$asString =~ s/([(,])(.*?):/${1}$hash{$2}:/g;
print "$asString\n";
close FH;
