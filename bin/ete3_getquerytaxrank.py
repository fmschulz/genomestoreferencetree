#!/usr/bin/env python
import sys
from ete3 import Tree
from collections import defaultdict
from collections import Counter

"""
Assign taxonomy to query sequences based on their position in a phylogenetic tree
June 2016, fschulz@lbl.gov
"""

# check command line arguments
if len(sys.argv[1:]) != 4:
	print ("usage: python ete3_getquerytaxrank.py <newicktree> <queryids> <taxonomymapping> <level[P|C|O|F]>")
	sys.exit(1)


# tree in newick format
TREE = sys.argv[1]
# file with query taxa short names, one per line
QUERY = sys.argv[2]
# lookuptable with shortnames of all reference sequences and taxonomy string
ASSIGNMENTS = sys.argv[3]
# taxonomic level, "P" for phylum, "C" for class , "O" for order, "F" for family
LEVEL = sys.argv[4]


# get lookuptable
lookup_dic = {}
with open (ASSIGNMENTS, "r") as infile:
	for line in infile:
		line = line.strip()
		if LEVEL == "P":
			try: 
				lookup_dic[line.split("\t")[0]] = line.split("|")[3]
			except IndexError:
				pass
		elif LEVEL == "C":
			try:
				lookup_dic[line.split("\t")[0]] = line.split("|")[4]
			except IndexError:
				pass
		elif LEVEL == "O":
			try:
				lookup_dic[line.split("\t")[0]] = line.split("|")[5]
			except IndexError:
				pass
		elif LEVEL == "F":
			try: 
				lookup_dic[line.split("\t")[0]] = line.split("|")[6]
			except IndexError:
				pass

			   


def update_lookuptable (tree, lookup_dic):
	"""
	add taxa which are in tree but have no assignment in lookupfile
	"""
	for node in t.traverse():
		if node.is_leaf() and node.name not in lookup_dic:
			lookup_dic[node.name] = "unknown"


def unpack_family(parentnode, leaves):
	"""
	get all leaves under a parental node
	"""
	children = parentnode.get_children()
	for child in children:
		if child.is_leaf():
			leaves.append(str(child).replace("--","").replace("\n",""))
		else:
			unpack_family(child, leaves) 
	return leaves


def get_assignment(node):
	"""
	#collect info for query sequence taxonomic classification from tree neighborhood
	"""
	# start with empty classification list, as it will be filled with all terminal nodes at each step
	classification = []
	# move up one node in the tree
	parentnode = node.up
	# collect all terminal leaves under the parent node
	leaves = []
	leaves = unpack_family(parentnode, leaves)
	# taxonomically classify leaves if possible
	for child in leaves:
		classification.append(lookup_dic[child])
	# return classification and parent node for additional iterations
	return parentnode, classification

# read tree with ete3
t = Tree(TREE)
# update lookup dictionary
update_lookuptable(t, lookup_dic)

# dictionary to harbor all query sequences as keys and corresponding classification as values
children_dic = defaultdict(list)
#children_dic = {}
with open(QUERY, "r") as infile:
	for line in infile:
		line = line.strip()
		# find query sequences in tree
		for node in t.traverse():
			if node.name == line:
				# start classification of unknown query sequence
				classification = ["unknown"]
				# classify neighboringing sequences in tree until assignment to known reference sequence is possible
				while classification.count("unknown") == len(classification):
					# function goes up one node and returns name this parental node and classification of all child nodes
					# this information gets iteratively updated if no clear classification is possible by going up one more node
					node, classification = get_assignment(node)
				# for each query sequence add classification to dictionary
				for x in classification:
					children_dic[line].append(x)

with open(QUERY + ".classified." + LEVEL, "w") as outfile:
	for target in sorted(children_dic.keys()):
		outfile.write (str(target) + "\t" + str(Counter(children_dic[target])).replace("Counter({", "").replace("})", "").replace("'", "").replace("Counter()", "none") + "\n")
