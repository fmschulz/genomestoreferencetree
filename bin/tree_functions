usage_main()
{
cat<<-EOF
  Usage: $0 [options] <directory of your genomes>

   required:
    <directory of your genomes>

   alignment options:
    <-a an alignment command[mafft|ete3|hmmalign]>
    <-m reference marker proteins [56COGs|RNAPol|RProt|RProtBF|gtdb_bac|gtdbbac]> 
    <-d reference database to use [refSSU85|refseqRNPOL90|gammaRNAPOL80|imggRNPOL65]>
    <-l generate tree from user supplied genomes only (i.e. makes multiple alignment of your genomes only)> 

	tree options:
    <-t a tree building command[fasttree|raxml|pplacer]>
    <-e (optional) supply an alignment file when you don't want to run an alignment with -a>

	global options
    <-p number threads [8] for multithreading on fasttree (and parallel jobs: mafft & prodigal)> 
    <-c to run the classifier> 
 
    * Running fasttree is faster but doesn't produce very good confidence values.
    * The number of threads is used for determining how many prodigal and mafft jobs can be run in parallel(limited to 8), as well as providing fasttree with number of threads (can be set larger than 8). RAxML is hard coded to use 16 threads on exclusive.c machine.
    * You can create a custom reference database.  The path to the extracted amino acids from a set of reference genomes can be created with <PROTEIN_EXTRACTOR.PY> and this directory would be used as the argument for "-d".
    
    example:
	### your genomes added to references
    # Runs mafft and then fasttree using your genomes in "Genomes".  Some jobs use "parallel GNU" and are limited here to 4 processes. 
    tree_wrapper_NERSC.sh -a mafft -t fasttree -m 56COGs -d refSSU85 -p 4 Genomes
    
	### tree of your genomes only
    # Using the -l flag will only align your genomes against each other and omit the reference genomes. 
    # Thus only your genomes will be on the tree. When you use the -l flag, you still need to include -d <database> 
    # since the marker set (i.e. 56COGs) is database dependent (i.e. there is a 56COGs set under databases refSSU85 and imggRNPOL65). 
    # By default, mafft and prodigal will be run in parallel using 8 processes. 
    tree_wrapper_NERSC.sh -a mafft -t fasttree -l -m 56COGS -d refSSU85 Genomes     
    
	### classify genomes in addition to tree 
    # using -c will classify your genomes based on where they land in the tree. This is incompatible 
    # with -l flag. You may include the -x argument specifying which taxa level to classify to
    # where phyla (P) is default but your choices are [P|C|O|F|G] for phyla,class,order,family & genus.  I'm classifying to class level here.
    tree_wrapper_NERSC.sh -a mafft -t fasttree -c -x C Genomes
    
	### if you already have alignment, run tree only. You can also just run the alignment.
    # this will run a tree only. You need to supply a relative path to a multiple alignment file with the -e flag. 
	# The alignment file's name format is what would be produced by the first example above (i.e. "<markers>_<database>.aln.trim.01.aln")
	tree_wrapper_NERSC.sh -t raxml -m 56COGs -d refSSU85 -e Genomes_tree/56COGs_refSSU85.aln.trim.01.aln Genomes 

	EOF
	exit 1
}

checkInPath()
{
	cmd=$1
	path=$(which $cmd)
	if [[ $? == 0 ]]; then 
		echo "## found $cmd ... $path"
	else
		echo "please add $cmd to \$PATH"
		exit 1
	fi
}

loadRequiredModules()
{
	CMD_ALIGN=$1
	CMD_TREE=$2

	#
	# lets first test global requirements
	#
	echo "## checking we have all dependencies"
	checkInPath python
	checkInPath prodigal
	checkInPath hmmalign
	checkInPath parallel
	

	# load alignment base modules
	if [[ $CMD_ALIGN == 'mafft' ]]; then
		checkInPath mafft
	elif [[ $CMD_ALIGN == 'ete3' ]]; then
		checkInPath ete3
	fi

	# load tree base modules
	if [[ $CMD_TREE == 'fasttree' ]]; then
		checkInPath FastTreeMP
		checkInPath trimal
	elif [[ $CMD_TREE == 'raxml' ]]; then
		checkInPath raxmlHPC-HYBRID-SSE3
		checkInPath trimal
#		checkInPath openmpi/1.8.7  # this module is currently loaded in runRAxML-hybrid.sh
#	elif [[ $CMD_TREE == 'pplacer' ]]; then
	fi

	# check biopython was found
	python -c "import Bio"
	if [[ $? > 0 ]]; then
		echo "The module \"Bio\" was not found by python.  Can you run \"module load biopython\"?"
		exit 1
	else
		echo "## found python module: Bio"
	fi
	echo "## end of pre-run checks ..."
}

extract_proteins()
{
	LOG=$1
	DIR=$2
	PROC=$3
	MARKERS=$4
	MYGENOMES_FAA=$5
	GOLD_HMMS=$6
	MYGENOMES=$7

    # Rename headers of query genome contigs
	# but first remove any old *.fna files which we don't want to use for the prodigal step.
	rm * faa *.fna 2> /dev/null || echo "## no old *.fna or *.faa files to delete."

    echo -n "## renaming headers ... "
    echo "## renaming headers ... " >> $LOG
    echo "$path_to_bin/rename_headers.py $DIR Q0" >> $LOG
    $path_to_bin/rename_headers.py $DIR Q0 2>> $LOG
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed to rename headers"
    	exit
    fi
    
    # Gene calling with prodigal (run in parallel)
	# this step will produce Q0.*.faa files (unless they were
	# already produced)
	checkForProteins=($(ls $DIR/*.faa 2> /dev/null))
	if [[ ! ${checkForProteins[@]} ]]; then
        echo -n "## predicting proteins ... "
        echo "## predicting proteins ... " >> $LOG
        echo "ls *.fna | cut -d'.' -f1 | parallel --jobs $PROC prodigal -i {}.fna -a {}.faa -p meta > /dev/null" >> $LOG
        	  ls *.fna | cut -d'.' -f1 | parallel --jobs $PROC prodigal -i {}.fna -a {}.faa -p meta > /dev/null 2>> $LOG
        if [[ $? == 0 ]]; then 
        	echo "success"
        else
        	echo "failed prodigal"
        	exit
        fi
		
    fi

    
    # Marker extraction / merging
    # Extract markers from genomes, only best hits
    #	* In case of multiple copies of the marker only the copy which fits best to the hmm is selected
    #	* If there are contaminant contigs, this step will bias the phylogenetic position of the query genome
    # Merge reference and genome markers
    mkdir Allgenomes Hits $MYGENOMES_FAA Merged 2> /dev/null 
    cat *.faa > Allgenomes/all.faa || exit 1
    mv *.faa Allgenomes || exit 1
    
    echo -n "## predicting hmms ... "
    echo "## predicting hmms ... " >> $LOG
    for x in $GOLD_HMMS/*.hmm; do 
		d=$(basename $x)
		prefix=${d%.hmm}	
        hmmsearch --noali --cut_ga --domtblout Hits/${prefix}_hmmHits.txt $x Allgenomes/all.faa 2>> $LOG > /dev/null
    done
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed hmmsearch $?"
    	exit
    fi
    
    echo -n "## extracting best hits ... "
    echo "## extracting best hits ... " >> $LOG
    $path_to_bin/extractbesthmmhits.py Hits Allgenomes >> $LOG 2>&1
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed to extract best hits"
    	exit
    fi
	if [[ -d $MYGENOMES_FAA ]]; then
    	mv *_hmmHits.faa $MYGENOMES_FAA
	else
		echo "Failed command: mv *_hmmHits.faa $MYGENOMES_FAA"
		exit 1
	fi
    
    echo -n "## concatenating proteins to one merged file ... "
    echo "## concatenating proteins to one merged file ... " >> $LOG
    while read line; do 
    	if [ $MYGENOMES ]; then
    		ln -s $(pwd)/$MYGENOMES_FAA/${line}_hmmHits.faa Merged/${line}.merged.faa
    	else
    		cat $MYGENOMES_FAA/${line}_hmmHits.faa $MARKERS/${line}_ref_hmmHits.faa > Merged/${line}.merged.faa
    	fi
    done < <(ls $GOLD_HMMS/* | awk -F "/" '{print $NF}' | sed 's/\.hmm//') >> $LOG 2>&1
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed to merge proteins"
    	exit
    fi
}        

run_mafft()
{
	LOG=$1
	ALIGNMENT=$2

    # Alignment
    # Quick alignment with MAFFT
    # or Better alignment with MAFFT-linsi, GISMO or consenus aligner (MAFFT, ClustalO, dialign, T-COFFEE)
    # Trimming ? e.g. trimal 0.1 to remove all positions with more than 10% gaps

	if [[ ! -d Alignments ]]; then
		mkdir Alignments
	else
		rm Alignments/*
	fi
    echo -n "## multiple alignment ... "
    echo "## multiple alignment ... " >> $LOG
    echo "ls Merged/*.faa | parallel --jobs $PROC \"mafft --quiet --thread 1 {} > Alignments/{/}.mafft.aln 2> /dev/null\"" >> $LOG
    	  ls Merged/*.faa | parallel --jobs $PROC  "mafft --quiet --thread 1 {} > Alignments/{/}.mafft.aln 2>> $LOG"
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed multiple alignment"
    	exit
    fi
    
    echo -n "## concatenating alignments ... "
    echo "## concatenating alignments ... " >> $LOG
    $path_to_bin/concatenate_alignments.py Alignments $ALIGNMENT >> $LOG 2>&1
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed multiple alignment"
    	exit
    fi
}

run_ete3()
{

	LOG=$1
	ALIGNMENT=$2

	if [ ! -d Alignments ]; then
		mkdir Alignments
	else
		rm Alignments/*
	fi

	ls Merged/*merged.faa > list
	if [[ $? > 0 ]]; then
		echo "Error: no files found in Merged/*merged.faa... exiting"
		echo "something probably went wrong with marker gene extraction step"
		exit
	fi
 
    count=$(wc -l list | cut -f1 -d' ')
    if [[ $count != 56 ]]; then
        echo "There are one or more missing *.merged.faa files in the list file. There were $count but should have been 56. This may be because some COGs were not found."
        echo
    fi
    
    # ------------------------------
    # run alignments  
    #
    echo "## multiple alignment ... "
    echo "## multiple alignment ... " >> $LOG
    echo   "qsub -t 1:$count $path_to_bin/ete3_align_wrapper.sh list 16" >> $LOG
    echo   "qsub -t 1:$count $path_to_bin/ete3_align_wrapper.sh list 16"
    JOBID=$(qsub -t 1:$count $path_to_bin/ete3_align_wrapper.sh list 16)
	if [[ $? > 0 ]]; then
		echo "exiting because qsub failed"
		exit 1
	fi
    # ------------------------------
    # now wait for job to finish
    JOBID=$(echo $JOBID | cut -f3 -d' ' | cut -f1 -d'.')
    echo "submitted $JOBID"    
    LIMIT=144 # Upper limit (will run job for 12hr before timeout) 
    a=0 
    while [ $a -le "$LIMIT" ]; do  
        isjobcomplete $JOBID
        if (( ! $? )); then 
          echo complete; 
          break
        fi  
    
        a=$(($a+1))
            echo "$a of $LIMIT before timeout (i.e. 12hrs)"
            if [ $a -eq "$LIMIT" ]; then
                echo "Warning: (JOBID=$JOBID) Timmed out"
                break
            fi
        sleep 300
    done


	# check if any alignment jobs failed and need a re-do
	for i in `$path_to_bin/checkFailures.sh $JOBID | sed '1,1 d' | cut -f2 -d',' | sort -n`; do
	  sed -n "$i,$i p" list
	done > redolist
	if [[ -s redolist ]]; then

cat<<EOF

########################################################################    
There seems to be some alignment array jobs that failed. Please look at the log files (aln.<jobid>) to determine nature of error.  You can rerun these jobs by using the following commands. Note that you need to substitute the appropriate JOBID. A "redolist" has already been created for you but I've included this code to re-generate it in case you need to re-run some alignments a second time: 

    cd <working_dir>_tree
    for i in \`$path_to_bin/checkFailures.sh <JOBID> | sed '1,1 d' | cut -f2 -d',' | sort -n\`; do
        sed -n "\$i,\$i p" list
    done > redolist
    count=\$(wc -l redolist | cut -f1 -d' ') 
    qsub -t 1:\$count -cwd -b yes -j yes -w e -r y $path_to_bin/ete3_align_wrapper.sh redolist 16


Once all the alignments are completed sucessfully, do:
    
    mkdir Alignments
    cp *_ETE/metaligner_default-trimal01-none-fasttree_full/*final_tree.trimmed.fa Alignments
    $path_to_bin/concatenate_alignments.py Alignments $ALIGNMENT

hopefully you end up with one alignment file named something like "*.aln"

now re-run the tree_wrapper_NERSC.sh with the appropriate tree command BUT WITHOUT the alignment command:
    cd ../  # got up one from <working_dir>_tree
i.e. if you ran
    $path_to_repository/tree_wrapper_NERSC.sh -a ete3 -t fasttree -m 56COGs -d refSSU85 Genomesdir

you should now run
    $path_to_repository/tree_wrapper_NERSC.sh -t fasttree -m 56COGs -d refSSU85 -e <path_to_your_alignment> Genomesdir

########################################################################    
EOF

		exit 1
	fi

	cp *_ETE/metaligner_default-trimal01-none-fasttree_full/*final_tree.trimmed.fa Alignments
    if [[ $? > 0 ]]; then 
		echo "final trimmed alignments (*_ETE/metaligner*/*final_tree.trimmed.fa) were not found"
		exit 1
	fi

    echo -n "## concatenating alignments ... "
    echo "## concatenating alignments ... " >> $LOG
    echo "concatenate_alignments.py Alignments $ALIGNMENT" >> $LOG
    $path_to_bin/concatenate_alignments.py Alignments $ALIGNMENT >> $LOG 2>&1
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed multiple alignment"
    	exit
    fi

}

run_hmmalign()
{
	LOG=$1
	MYGENOMES_FAA=$2
	GOLD_HMMS=$3
	ALIGNMENT=$4

	if [[ ! -d $MYGENOMES_FAA ]]; then
		echo "Missing directory \$MYGENOMES_FAA: $MYGENOMES_FAA"
		exit 1
	fi
	if [[ ! -s $GOLD_HMMS ]]; then 
		echo "Missing or empty file \$GOLD_HMMS: $GOLD_HMMS"
		exit 1
	fi

    # Alignment
	if [[ ! -d Alignments ]]; then
		mkdir Alignments
	else
		rm Alignments/* 2> /dev/null || echo no alignment files to remove
	fi
    echo -n "## multiple alignment ... "
    echo "## multiple alignment ... " >> $LOG
	$path_to_bin/hmmalignmarkers_initial.py Merged $GOLD_HMMS >> $LOG 2>&1
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed multiple alignment"
    	exit 1
    fi
    
	if [[ -d Alignments ]]; then
		mv *.01.aln Alignments
		rm *.aln *.sto
	else
		echo "failed command: mv *.01.aln Alignments"
		exit 1
	fi

    echo -n "## concatenating alignments ... "
    echo "## concatenating alignments ... " >> $LOG
    $path_to_bin/concatenate_alignments.py Alignments $ALIGNMENT >> $LOG 2>&1
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed multiple alignment"
    	exit
    fi
}


run_fasttree()
{
	LOG=$1
	ALIGNMENT=$2
	THREADS=$3

	export OMP_NUM_THREADS=$THREADS

	if [[ ! -s $ALIGNMENT ]]; then
		echo "run_fasttree: Missing or empty alignment file: $ALIGNMENT"
		echo "run one of the alignment steps first"
		exit
	fi

    echo -n "## building tree ... "
    echo "## building tree ..." >> $LOG
    echo "trimal -in $ALIGNMENT -out $ALIGNMENT.trim.01.aln -gt 0.1 " >> $LOG
     	  trimal -in $ALIGNMENT -out $ALIGNMENT.trim.01.aln -gt 0.1 
    echo "FastTreeMP -quiet -pseudo -spr 4 -mlacc 2 -slownni -wag < $ALIGNMENT.trim.01.aln > final.01.GTR.nwk" >> $LOG
    	  FastTreeMP -quiet -pseudo -spr 4 -mlacc 2 -slownni -wag < $ALIGNMENT.trim.01.aln > final.01.GTR.nwk 2>> $LOG
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed to create tree"
    	exit 1
    fi
}


run_raxml()
{
	LOG=$1
	ALIGNMENT=$2
	
    echo "## building tree ... "
    echo "## building tree ..." >> $LOG
	echo "trimal -in $ALIGNMENT -out $ALIGNMENT.trim.01.aln -gt 0.1 "
	      trimal -in $ALIGNMENT -out $ALIGNMENT.trim.01.aln -gt 0.1 

	# RAxML with 100 rapid bootstrap replications, submit to cluster
	echo "qsub -l h_rt=24:0:0 runRAxML-hybrid.sh concat.trim.01.faa nwk1" >> $LOG
	echo "qsub -l h_rt=24:0:0 runRAxML-hybrid.sh concat.trim.01.faa nwk1"
		  qsub -l h_rt=12:0:0 $path_to_bin/runRAxML-hybrid.sh concat.trim.01.faa nwk1

	echo "qsub -l h_rt=48:0:0 runRAxML-hybrid.sh concat.trim.01.faa nwk2" >> $LOG
	echo "qsub -l h_rt=48:0:0 runRAxML-hybrid.sh concat.trim.01.faa nwk2"
		  qsub -l h_rt=12:0:0 $path_to_bin/runRAxML-hybrid.sh concat.trim.01.faa nwk2

	echo "# ---------------------------------------------------------------------------------------------- #"
	echo "# Once these jobs are finished, you need to run the following command to add                     #"
	echo "# the bootstrap values to the best tree. You should end up with two final                        #"
	echo "# trees called RAxML_bipartitions.nwk3 and RAxML_bipartitionsBranchLabels.nwk3.                  #"
	echo "#                                                                                                #" echo "# combine bootstap values to best tree                                                           #"
	echo "# $path_to_bin/runRAxML-hybrid.sh concat.trim.01.phy nwk3 "
	echo "# ---------------------------------------------------------------------------------------------- #"
	echo

}

run_classifier()
{
	LOG=$1
	TREE=$2
	QUERYNAMES=$3
	LOOKUP=$4

    echo -n "## classifying ... "
    echo "## classifying ..." >> $LOG
    echo "$path_to_bin/parseQueryNames.py $TREE > $QUERYNAMES" >> $LOG
          $path_to_bin/parseQueryNames.py $TREE > $QUERYNAMES
    if [[ $? > 0 ]]; then
        echo "Error in $path_to_bin/parseQueryNames.py when trying to parse node names from tree: $TREE"
        exit 1
    fi 

	echo "$path_to_bin/ete3_getquerytaxrank_singlequery_V2.py $TREE $QUERYNAMES $LOOKUP" > allClassifications >> $LOG
	      $path_to_bin/ete3_getquerytaxrank_singlequery_V2.py $TREE $QUERYNAMES $LOOKUP >  allClassifications 
    if [[ $? == 0 ]]; then 
    	echo "success"
    else
    	echo "failed to classify"
    	exit 1
    fi
}

run_treePlotter()
{
	TREE=$1
	REF_LOOKUP=$2
#	COLORS=$3

    echo -n "## plotting tree as pdf ... "
    echo "## plotting tree as pdf ..." >> $LOG
    # we need to make a lookup table that concatenates reference genomes and user query genomes
    if [[ ! -s "Q0_taxalookup.tab" ]]; then
        echo "Missing or empty lookup table: Q0_taxalookup.tab"
        exit 1
    fi


    if [ -e $TREE ] && [ -e $REF_LOOKUP ]; then
    	cat $REF_LOOKUP Q0_taxalookup.tab > ${dir_bname}_lookup.tab
        $path_to_bin/plot_tree_ete3_V2.py $TREE ${dir_bname}_lookup.tab
        if [[ $? == 0 ]]; then
			echo "success"
		else
            echo "Error running $path_to_bin/plot_tree_ete3_V2.py $TREE ${dir_bname}_lookup.tab"
            exit 1
        fi  
    else
        echo "Didn't try and plot tree since either there wasn't a tree, a lookup table"
#        echo reflookup $REF_LOOKUP 
#		echo concat lookup Q0_taxalookup.tab
#        echo tree ${TREE[0]}
#        echo lookup $LOOKUP
#        echo colors $COLORS
    fi
}

checkLegitimacy()
{
	DATABASE=$1
	echo "Your database is: $DATABASE and there is no validation yet"
	echo "Please don't use this option....exiting"
	echo "You need to restrict your argument to one of these [refSSU85|refseqRNPOL90|<custom>]."
	echo "Refer to help for more information about choices."
	echo 1
}
