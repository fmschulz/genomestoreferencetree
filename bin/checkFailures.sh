#!/bin/bash
set -e
JOBID=$1
if [[ ! $JOBID ]]; then
	echo
	echo "Usage: $0 <job id>"
	echo "will query qqacct"
	echo 
	exit
fi
qqacct -q job_number==$JOBID | awk -F',' '$10 !=0 || $11 != 0'
