#!/bin/bash -l
#$ -cwd -b yes -j yes -w e -r y
#$ -l exclusive.c
#$ -l h_rt=12:00:00 
#$ -pe pe_16 320
#$ -N raxml

align=$1
suffix=$2
if [ ! $align ] || [ ! $suffix ]; then
	echo " "
	echo "To generate 20 bootstrap trees or a best tree..."
	echo "Usage: qsub $0 <alignment in fasta|pylip> <suffix[nwk1|nwk2]> "
	echo "  use nwk1 to create 20 trees and RAxML_bestTree.nwk1"
	echo "  use nwk2 to create 100 trees and RAxML_bootstrap.nwk2"
	echo " "
	echo "Or to combine bootstraps to best tree. You don't need a qsub job for this."
	echo "Usage: $0 <alignment in fasta|pylip> <nwk3> "
	echo " "
	exit
fi
module load openmpi/1.8.7 
# this will have to be changed eventually. Maybe have people move their own copy to repository.
#export PATH=/global/u2/j/jfroula/bin/RAxML.8.2.8:$PATH
module load RAxML/8.2.2

if [[ $suffix == 'nwk1' ]]; then
	mpirun -np 20 --map-by node raxmlHPC-HYBRID-SSE3 -p 12345 -s $align -# 20 -m PROTCATLG -n $suffix -C -T 16
elif [[ $suffix == 'nwk2' ]]; then
	mpirun -np 20 --map-by node raxmlHPC-HYBRID-SSE3 -m PROTCATLG -p 12345 -b 12345 -# 100 -s $align -n $suffix -C -T 16
elif [[ $suffix == 'nwk3' ]]; then
	raxmlHPC-SSE3 -m PROTCATLG -p 12345 -f b -t RAxML_bestTree.nwk1 -z RAxML_bootstrap.nwk2 -n $suffix -C 
else
	echo "please use [nwk1|nwk2|nwk3] for suffix"
	exit
fi
