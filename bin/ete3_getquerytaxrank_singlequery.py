#!/usr/bin/env python
import sys
from ete3 import Tree
from collections import defaultdict
from collections import Counter

"""
Assign taxonomy to query sequences based on their position in a phylogenetic tree
June 2016, fschulz@lbl.gov
"""

# check command line arguments
if len(sys.argv[1:]) != 3:
    print ("usage: python ete3_getquerytaxrank.py <newicktree> <queryids> <taxonomymapping>")
    sys.exit(1)


# tree in newick format
TREE = sys.argv[1]
# file with query taxa short names, one per line
QUERY = str(sys.argv[2])
# lookuptable with shortnames of all reference sequences and taxonomy string
ASSIGNMENTS = sys.argv[3]

# get lookuptable
lookup_dic = {}
with open (ASSIGNMENTS, "r") as infile:
    for line in infile:
        line = line.strip()
        lookup_dic[line.split("\t")[0]] = line.split("\t")[1]


def unpack_family(parentnode, leaves):
    """
    get all leaves under a parental node
    """
    children = parentnode.get_children()
    for child in children:
        if child.is_leaf():
            leaves.append(str(child).replace("--","").replace("\n",""))
        else:
            unpack_family(child, leaves) 
    return leaves


# read tree with ete3
t = Tree(TREE)
for node in t.traverse():
    if node.name == QUERY:
        # move up one node in the tree
        parentnode = node.up
        # collect all terminal leaves under the parent node
        leaves = []
        leaves = unpack_family(parentnode, leaves)
        for child in leaves:
            if child != QUERY:
                print (QUERY + "\t" + child + "\t" + lookup_dic[child])
