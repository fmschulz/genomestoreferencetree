#!/usr/bin/env python
from __future__ import print_function

from Bio import SeqIO
from Bio.Seq import Seq, UnknownSeq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment
from Bio.Alphabet import IUPAC
from collections import defaultdict

import glob
import sys

if len(sys.argv) < 3:
    print("Usage: sys.argv[0] <alignments dir> <resulting alignment file>")
    sys.exit()

#directory containing all alignments
alignmentsdir=sys.argv[1]

# results will be written to this file
alignment_file=sys.argv[2]


#-----------------read in alignments---------------

aln_out=[]
for aln in glob.glob(alignmentsdir+"/*"):
    aln_out.append(aln)

#-----------------shape alignments----------------------

z=1
sequences = {}
for aln in aln_out:
    multialn={}
    for seq_record in SeqIO.parse(aln, "fasta"):
        multialn[str(seq_record.id).split("|")[0]]=str(seq_record.seq)
    sequences["aln"+str(z)] =  multialn
    z+=1

alignments = [MultipleSeqAlignment([SeqRecord(Seq(sequence, alphabet=IUPAC.extended_protein), id=key) 
                   for (key, sequence) in sequences[aln].items()])
                       for aln in sequences]

#--------------------concatenate------------------------------

def concatenate(alignments):
    """
    Concatenates a list of Bio.Align.MultipleSeqAlignment objects.
    If any sequences are missing they are padded with 'X'.
    Returns a single Bio.Align.MultipleSeqAlignment.
    """
    
    # Get the full set of labels (i.e. sequence ids) for all the alignments
    all_labels = set(seq.id for aln in alignments for seq in aln)    
    # Make a dictionary to store info as we go along
    tmp = defaultdict(list)
    # Assume all alignments have same alphabet
    alphabet = alignments[0]._alphabet
    
    for aln in alignments:
        length = aln.get_alignment_length() 
        # check if any labels are missing in the current alignment
        these_labels = set(rec.id for rec in aln)
        missing = all_labels - these_labels
        # if any are missing, create unknown data of the right length,
        # stuff the string representation into the tmp dict
        for label in missing:
            new_seq = UnknownSeq(length, alphabet=alphabet)
            tmp[label].append(str(new_seq))
        # else stuff the string representation into the tmp dict
        for rec in aln:
            tmp[rec.id].append(str(rec.seq))
            
    # Stitch all the substrings together using join (most efficient way),
    # and build the Biopython data structures Seq, SeqRecord and MultipleSeqAlignment
    return MultipleSeqAlignment(SeqRecord(Seq(''.join(v), alphabet=alphabet), id=k) 
               for (k,v) in tmp.items())

msa = concatenate(alignments)

with open(alignment_file,"w") as outfile:
    for alnseq in msa:
        outfile.write(">"+str(alnseq.id)+"\n"+str(alnseq.seq)+"\n")
