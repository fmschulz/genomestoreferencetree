#!/usr/bin/env python
import sys
import glob, os
from Bio import SeqIO
import string
import random

if len(sys.argv) < 3:
    print ("Usage: "+sys.argv[0]+" <multifaadir> <prefix>")
    sys.exit()

#dir with all multifaa files, header names can be long and ugly
multifaadir = str(sys.argv[1])
#prefix for the shortname, e.g. A0 for archaea, B0 for bacteria, E0 for eukaryotes, Q0 for query sequences
PREFIX = str(sys.argv[2])

#random name generator
seen=[]
def id_generator(size = 6, chars = string.ascii_uppercase):
    randy = ''.join(random.choice(chars) for _ in range(size))
    if randy not in seen:
        seen.append(randy)
    else:
        id_generator()
    return randy

#rename taxa to generate n character abbrevation neccessary for orthomcl or just in general nicer to work with
def renameTaxa(proteomeslist):
    taxalookup = {}
    taxalookup_rev = {}
    for x in proteomeslist:
        x = x.split("/")[-1]
        y = id_generator()
        taxalookup[PREFIX + y] = str(x)
        taxalookup_rev[x] = PREFIX + y
    return taxalookup, taxalookup_rev


#rename proteins to make them orthomcl conform, prevent duplicates!
def renameProteins(proteomeslist,extension):
    if extension == '.faa':
        suffix='faa'
    elif extension == '.fna' or extension == '.fa' or extension == '.fasta' or extension == '.fas':
        suffix='fna'
    else:
        print ("The file extension used ("+extension+") for your genomes files are not recognized. They should be [fasta|fa|fna|fas] if nucleotide and [faa] if protein")
        sys.exit()

    protslookup = {}
    for x in proteomeslist:
        y = x.split("/")[-1]
        z = 1
        seenseq = []
        with open(taxalookup_rev[y]+'.'+suffix, 'w') as outfile:
            for seq_record in SeqIO.parse(x, "fasta"):
                outfile.write(str(">" + taxalookup_rev[y] + '|' + taxalookup_rev[y] +  '_' + str(z) + "\n" + seq_record.seq + "\n"))
                protslookup[taxalookup_rev[y] + '|' + taxalookup_rev[y] +  '_' + str(z)] = seq_record.description
                z += 1
    return protslookup


allproteomes = []
for file in glob.glob(multifaadir + "/*"):
    allproteomes.append(file)


taxalookup, taxalookup_rev = renameTaxa(allproteomes)

extension = os.path.splitext(allproteomes[0])[1]
protslookup = renameProteins(allproteomes,extension)

#export lookup files
with open(PREFIX + '_protslookup.tab', 'w') as outfile:
    for x in sorted(protslookup):
        outfile.write(x + "\t" + protslookup[x] + "\n")
        
with open(PREFIX + '_taxalookup.tab', 'w') as outfile:
    for x in sorted(taxalookup_rev):
        outfile.write(taxalookup_rev[x] + "\t"  + x.rsplit(".", 1)[0] + "\n")
