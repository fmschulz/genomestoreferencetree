#!/usr/bin/env python
import sys
import os
import re

if(len(sys.argv) < 1):
    print "You must set argument (i.e. your tree in nwk format)!!!"
    sys.exit(1)

if not os.path.exists(sys.argv[1]):
    print "Error: %s does not exist" % sys.argv[1]
    sys.exit(1)
    
try:
    FH=open(sys.argv[1],'r')
    data=FH.readlines()
    FH.close()
except IOError, e:
    print 'file open error:', e
    sys.exit(1)

for eachLine in data:
    "parse a newick tree for the node names."
    "They should come after a comma or left parenthesis and before colon."

    eachLine.rstrip() # get rid of end-of-line

    obj = re.findall(r'[,(](.*?):',eachLine)
    for sub in obj:
        subobj = re.sub(r'[(]', "", sub)
        print subobj
