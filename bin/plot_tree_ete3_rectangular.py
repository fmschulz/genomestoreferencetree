#!/usr/bin/env python
import re
import sys
from ete3 import Tree, faces, CircleFace, AttrFace, TreeStyle, NodeStyle, TextFace
import ete3 as ete

"""
Plot and modify newick tree with ete3
June 2016, fschulz@lbl.gov
"""

# check command line arguments
if len(sys.argv[1:]) != 3:
    print ("usage: python plot_tree_ete3.py <newicktree> <definitionsfile> phyla_colors.tab")
    sys.exit(1)

# Tree file in newick format
TREE=sys.argv[1]

# Metadata file 
DEFIN = sys.argv[2]

# Color definitions for phyla
PHYLACOLOR = sys.argv[3]

#collapse branches with lower support
support_cutoff = 0.5

#---------------------Prepare input------------------------------------------------

# define colors for phyla
with open(PHYLACOLOR, "r") as infile:
    phylacolorhash = {}
    for line in infile:
        line = line.strip()
        phylacolorhash[line.split()[0]] = line.split()[1]

def update_outgroup(outgroup, t):
    """
    update outgroup taxa to only include taxa present in the tree
    """
    updoutgroup = []
    for node in t.traverse():
        if node.is_leaf() and node.name in outgroup:
            updoutgroup.append(node.name)
    return updoutgroup


def get_defs(definfile):
	"""
	read input from configuration file:
	short and full names, tab separated, one per line
	additional lines my contain:
	outgroup:list of short names, space separated, these taxa will be set as outgroup
	remove:list of short names, space separated,  these taxa will be removed from the tree
	highlight:list of short names, space separated, these taxa will be highlighted by colored circles
	"""
	taxacolorhash = {}
	nameshash = {}    
	highlight = []
	removetaxa = []
	outgroup = []
	with open(definfile, "r") as infile:
		for line in infile:
			line = line.strip()
			if "\t" in line:
				if ";" in line:
					nameshash[line.split("\t")[0]] = line.split("|")[-1]
					for x in re.split(' |\||;|_|\(|\)', line):
						if x in phylacolorhash:
							taxacolorhash[line.split("\t")[0]] = phylacolorhash[x]
							break
				else:
					nameshash[line.split("\t")[0]] = line.split("\t")[1].replace("_", " ")
					# grey if no color has been assigned in PHYLACOLOR
					taxacolorhash[line.split("\t")[0]] = "#727272"

			elif line.startswith("highlight:"):
				highlight = line.split(":")[1].split()

			elif line.startswith("remove:"): 
				removetaxa = line.split(":")[1].split()

			elif line.startswith("outgroup:"):
				outgroup = line.split(":")[1].split()

	return taxacolorhash, nameshash, highlight, removetaxa, outgroup


taxacolorhash, nameshash, highlight, removetaxa, outgroup = get_defs(DEFIN)

#--------------------------Plot the tree-----------------------------------------------


def FS_layout(node):
	"""
	Define plotting style, visit leave nodes and add fullnames, branches colored based on taxonomy string, highlights
	"""
	node.img_style["size"] = 0
	color = "#727272"
	if node.is_leaf():
		
		if str(node.taxname) in taxacolorhash:
			color = taxacolorhash[str(node.taxname)]
			taxonNode = ete.faces.TextFace(nameshash[node.taxname] + node.taxname, fgcolor = taxacolorhash[str(node.taxname)], fsize = 70, fstyle = 'bold')
			if str(node.taxname) in highlight:
				C1 = CircleFace(radius = 80, color = "red", style = "circle")
				C1.opacity = 0.7
				ete.faces.add_face_to_node(C1, node, 2, position = "branch-right")
			ete.faces.add_face_to_node(taxonNode, node, 0)
               
		else:
			taxonNode = ete.faces.TextFace(nameshash[node.taxname] + node.taxname, fgcolor = "#727272", fsize = 70, fstyle = 'bold')
			if str(node.taxname) in highlight:
				C1 = CircleFace(radius = 80, color = "red", style = "circle")
				C1.opacity = 0.7
				ete.faces.add_face_to_node(C1, node, 2, position = "branch-right")
			ete.faces.add_face_to_node(taxonNode, node, 0)
   
	else:
		common_name = list(node2taxnames[node])[0]
		color = taxacolorhash.get(str(common_name), "#727272")
		if "support" in node.features:
		   supportvalue = ete.faces.TextFace(str(node.support),fgcolor = "black", fsize = 60)
		   ete.faces.add_face_to_node(supportvalue, node, 0, position="branch-right")
    
	node.img_style["fgcolor"] = color
	node.img_style["vt_line_color"] = color
	node.img_style["hz_line_color"] = color
	node.img_style['hz_line_width'] = 50
	node.img_style['vt_line_width'] = 50

#---------------------------------------------------------------------------------------

# read tree
t = Tree(TREE)

#do not show root branch
t.dist = 0

#---------------------------------------------------------------------------------------


# set outgroup
finaloutgroup = update_outgroup(outgroup, t)

if len(finaloutgroup) >= 1:
	ancestor = t.get_common_ancestor(finaloutgroup)
	ancestorleaves=[]
	for node in ancestor.traverse():
		if node.is_leaf():
			ancestorleaves.append(node.name)
	if len(ancestorleaves) == len(finaloutgroup):
		t.set_outgroup(ancestor)
	else:
		print ("Outgroup not monophyletic")


# prune tree
keep = []
for node in t.traverse():
	if node.is_leaf():
		# also add names of leaves which might have been forgotten in definition file
		if node.name not in nameshash:
			nameshash[str(node.name)] = str(node.name)
		if node.name not in removetaxa:
			keep.append(node.name)
#Prune the tree in order to keep only some leaf nodes; define taxa which should remain in tree
t.prune(keep)

#-------------------------------

'''
#collapse below 50% support
for node in t.get_descendants():
    if not node.is_leaf() and node.support <= support_cutoff:
        node.delete()
'''

# beautify tree
t.ladderize()

ts = TreeStyle()
ts.mode = 'r'
ts.min_leaf_separation = 1
ts.layout_fn = FS_layout
ts.show_leaf_name = False
ts.allow_face_overlap = True
ts.scale = 1000

for leaf in t.iter_leaves():
	pname = leaf.name
	leaf.add_features(taxname=pname)

# cache the content of internal nodes for faster plotting
node2taxnames = t.get_cached_content(store_attr="taxname")

t.render(TREE.split(".")[0]+".pdf", w=5000, units="px", tree_style=ts)
