#!/usr/bin/env python
from Bio import SeqIO
import sys
import glob
import os.path

# directory containing the hmm tabular output, one for each marker
hmmoutdir = sys.argv[1].split("/")[0] 
#directory harboring all multifaa files
compliantFasta = sys.argv[2]

def getbesthits (hmmout):
    """Find hmm hits with highest sequence score"""
    besthitIDs = []
    seen = []
    with open (hmmout, "r") as infile:
        for line in infile:
            line = line.strip()
            if not line.startswith("#"):
                if line.split("|")[0] not in seen:
                    seen.append(line.split("|")[0])
                    besthitIDs.append(line.split()[0])
    return besthitIDs


def extractseq (besthitIDs, cogname):
    """Extract aminoacid sequences of best hits"""
    with open (cogname + ".faa", 'w') as outfile:
        for prot in besthitIDs:
            if os.path.isfile(compliantFasta + "/" +prot.split("|")[0]+".faa"):
                for seq_record in SeqIO.parse (compliantFasta + "/" +prot.split("|")[0]+".faa", "fasta"):
                    if prot == str(seq_record.id):
                        outfile.write (str (">" + seq_record.id + "\n" + seq_record.seq + "\n"))


for hmmout in glob.glob (hmmoutdir+"/*.txt"):
    cogname = hmmout.split("/")[-1].split(".")[0]
    extractseq (getbesthits (hmmout), cogname)
