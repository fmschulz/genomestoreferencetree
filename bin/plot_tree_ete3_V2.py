#!/usr/bin/env python
import re
from math import log
import sys
from ete3 import Tree, faces, CircleFace, AttrFace, TreeStyle, NodeStyle, TextFace, COLOR_SCHEMES
import ete3 as ete

# check command line arguments
if len(sys.argv[1:]) < 2:
    print ("usage: python plot_tree_ete3.py <newicktree> <definitionsfile> <<output format[pdf|svg|png]]>>")
    sys.exit(1)

try:
    TREE   = sys.argv[1]
    DEFIN  = sys.argv[2]
    FORMAT = sys.argv[3]
except IndexError:
    FORMAT = "pdf"


if FORMAT not in ['pdf','svg','png']:
    print ("output format must be set to pdf|svg|png")
    sys.exit(1)

# Setup for tree plotting: taxon full names, color definitions, outgroup, remove taxa, show support or not
def get_defs(definfile):
    nameshash = {}
    colorhash = {}
    removetaxa = {}
    support = "no"	
    outgroup = []
    with open(definfile, "r") as infile:
        for line in infile:
            line = line.strip()
            if "\t" in line:
                nameshash[line.split("\t")[0]]=line.split("\t")[1].replace("_", " ") + "_______" + str(line.split("\t")[0])
            elif line.startswith("remove:"): 
                removetaxa = line.split(":")[1].split()
            elif line.startswith("color:"):
                applyfor = line.split(":")[2].split()
                for x in applyfor:
                    colorhash[x] = line.split(":")[1]
            elif line.startswith("outgroup:"):
                outgroup = line.split(":")[1].split()
            elif line.startswith("support:"):
                support = line.split(":")[1].replace(" ", "")
    return nameshash, removetaxa, colorhash, outgroup, support

nameshash, removetaxa, colorhash, outgroup, support = get_defs(DEFIN)

# very basic tree layout
def FS_layout(node):
    node.img_style["size"] = 0
    color = "#000000"
    if node.is_leaf():
        #if node.taxname in pie_dic and node.name in colorhash and node.taxname in nameshash:
        if node.taxname in colorhash and node.taxname in nameshash:
            color = colorhash[node.taxname]
            taxonNode = ete.faces.TextFace(nameshash[node.taxname], fgcolor = colorhash[node.taxname], fsize = 100, fstyle = 'bold')
            ete.faces.add_face_to_node(taxonNode, node, 1)

        else:
            color= "#000000"
            taxonNode = ete.faces.TextFace(nameshash[node.taxname], fgcolor = "#000000", fsize = 100, fstyle = 'bold')
            ete.faces.add_face_to_node(taxonNode, node, 0)

    else:
        common_name = list(node2taxnames[node])[0]
        color = colorhash.get(str(common_name), "#000000")

        if "support" in node.features and support == "yes":
           supportvalue = ete.faces.TextFace(str(node.support),fgcolor = "black", fsize = 60)
           ete.faces.add_face_to_node(supportvalue, node, 0, position="branch-right")

    node.img_style["fgcolor"] = color
    node.img_style["vt_line_color"] = color
    node.img_style["hz_line_color"] = color
    node.img_style['hz_line_width'] = 20
    node.img_style['vt_line_width'] = 20



t = Tree(TREE)

t.allow_face_overlap = True
t.dist = 0 # root branch not be shown

#---------------------------------------------------------------------------------------

allleaves=[]
for node in t.traverse():
    if node.is_leaf():
        allleaves.append(node.name)
        if node.name in nameshash:
            pass
        else:
            nameshash[node.name]=node.name

finaloutgroup=[]
for x in allleaves:
    if x in outgroup:
        finaloutgroup.append(x)

print outgroup
print finaloutgroup

if len(finaloutgroup) >= 1:
    ancestor = t.get_common_ancestor(finaloutgroup)
    ancestorleaves=[]
    for node in ancestor.traverse():
        if node.is_leaf():
            ancestorleaves.append(node.name)
    if len(ancestorleaves) == len(outgroup):
        print ("outgroup set")
        t.set_outgroup(ancestor)
    else:
        print ("Outgroup not monophyletic")

#-------------------------------
'''
# collapse below 50% support
for node in t.get_descendants():
    if not node.is_leaf() and node.support <= support_cutoff:
        node.delete()
'''
t.ladderize()

# prune tree
keep = []
for node in t.traverse():
	if node.is_leaf():
		# also add names of leaves which might have been forgotten in definition file
		if node.name not in nameshash:
			nameshash[str(node.name)] = str(node.name)
		if node.name not in removetaxa:
			keep.append(node.name)

# Prune the tree in order to keep only some leaf nodes; 
# define taxa which should remain in tree
t.prune(keep)

### Some variables that user can change
### ---------------------------------------- ###
# change these variables to alter tree format
ts = TreeStyle()
ts.mode = 'c'                   # r=flat tree & c=circular
ts.min_leaf_separation = 1      # 
ts.branch_vertical_margin = 100  # 10 pixels between adjacent branches
# ts.rotation = 90              # rotates tree 90 degrees (appropriate or flat trees)
ts.layout_fn = FS_layout        #
ts.show_leaf_name = True        #
ts.allow_face_overlap = True    #
ts.scale = 10000                # pixels per branch length unit
ts.show_branch_length = False   #
ts.show_branch_support = False
#ts.arc_start = -180 # 0 degrees = 3 o'clock
#ts.arc_span = 180

### ---------------------------------------- ###

for leaf in t.iter_leaves():
    pname = leaf.name
    leaf.add_features(taxname=pname)

# cache the content of internal nodes
node2taxnames = t.get_cached_content(store_attr="taxname")

# show distribution of branch lengths
# print sorted([n.dist for n in t.traverse()])


if FORMAT == "pdf":
    t.render(TREE.split(".")[0]+"_ete3.pdf", w=2000, units="px", tree_style=ts)
elif FORMAT == "png":
    t.render(TREE.split(".")[0]+"_ete3.png", w=2000, units="px", tree_style=ts)
elif FORMAT == "svg":
    t.render(TREE.split(".")[0]+"_ete3.svg", w=4000, units="px", tree_style=ts)
